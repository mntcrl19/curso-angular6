import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViajes } from './../models/destino-viajes.models';

@Component({
  selector: 'app-destino-viajes',
  templateUrl: './destino-viajes.component.html',
  styleUrls: ['./destino-viajes.component.css']
})
export class DestinoViajesComponent implements OnInit {

  @Input() destinos: DestinoViajes;
  @HostBinding('attr.class') cssClass="col-md-4";
  @Output() clicked: EventEmitter<DestinoViajes>;
  @Input('idx') position: number;

  constructor() {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destinos);
    return false
  }

}
