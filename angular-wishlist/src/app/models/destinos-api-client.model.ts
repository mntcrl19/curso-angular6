import { DestinoViajes } from './destino-viajes.models';

export class DestinosApiClient {
	destinos:DestinoViajes[];
	constructor() {
       this.destinos = [];
	}
	add(d:DestinoViajes){
	  this.destinos.push(d);
	}
	getAll(){
	  return this.destinos;
    }
}