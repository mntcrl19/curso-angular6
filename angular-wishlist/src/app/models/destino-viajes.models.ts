/*export class DestinoViajes{
     nombre: string;
     imagenUrl: string;

     constructor(nom: string, imgUrl: string){
          this.nombre=nom;
          this.imagenUrl=imgUrl;
     }
} */

//esta linea de codigo hace exactamnte lo mismo que la de arriba
export class DestinoViajes{
    //variable
     private selected: boolean;

     //Array de strings
     public servicios: string[];

     constructor(public nombre: string, public imagenUrl: string){
          this.servicios = ['Desayuno', 'Cena']
     }

     //Metodo para identificar si esta seleccionado o no
     isSelected(): boolean{
     return this.selected;
     }

     //Metodo para marcarlo como seleccionado
     setSelected(s: boolean){
     this.selected=s;
     }
}