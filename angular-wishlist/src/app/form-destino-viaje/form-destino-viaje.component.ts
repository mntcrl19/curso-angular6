import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Form, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { DestinoViajes } from '../models/destino-viajes.models';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
@Output() onItemAdded: EventEmitter<DestinoViajes>;
fg: FormGroup;
minLongNombre = 3;

  constructor(fb: FormBuilder) {
    //inicializador
    this.onItemAdded = new EventEmitter();
    //vinculacion con tag html
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongNombre)
      ])],
      url: ['', Validators.required]
    });

    //observador de tipo
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio en el formulario: ', form)
    });
   }

  ngOnInit(): void {
  }

  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViajes(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  //Validadores Custom
  nombreValidator(control: FormControl): {[s: string]: boolean} {
    const longuitud = control.value.toString().trim().length;
    if (longuitud > 0 && longuitud < 5){
      return { invalidNombre: true};
    }
    return null;
  }

  //Agregar Validador Parametrizable
  nombreValidatorParametrizable(minLong: number): ValidatorFn{
    return (control: FormControl): {[s: string]: boolean} | null=> {
      const longuitud = control.value.toString().trim().length;
      if (longuitud > 0 && longuitud < minLong){
        return { minLongNombre: true};
      }
      return null
    }
  }

}
